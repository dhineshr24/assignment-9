from conditional import Conditional
import unittest
from unittest.mock import MagicMock

# def test_can_instantiate_calculator():
#     con = Conditional()

# @pytest.fixture()
#     def check():
#         con = Conditional()
#         return con


class TestUser(unittest.TestCase):

    def setUp(self):
        self.con = Conditional()

    def test_can_instantiate_calculator(self):
        con = Conditional()

    def test_check_int(self):
        # con = Conditional()
        with self.assertRaises(TypeError):
            self.con.check(1.5)

    def test_odd(self):
        # con = Conditional()
        assert self.con.check(3) == "Just odd"

    # def test_even(check):
    #     # con = Conditional()
    #     assert check.check(2) == "Just even"

    def test_even_range1(self):
        # con = Conditional()
        assert self.con.check(4) == "Even and between [2-5]"

    def test_even_range2(self):
        # con = Conditional()
        assert self.con.check(10) == "Even and between [6-20]"

    def test_even_range3(self):
        # con = Conditional()
        assert self.con.check(22) == "Even and greater than 20"

    def tearDown(self):
        self.con.greet_bye()


if __name__ == '__main__':
    unittest.main()
